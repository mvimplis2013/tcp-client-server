import pytest

from clientserver.client import connect_remote

@pytest.fixture
def get_connection(request):
        host, port = request.param 
        return connect_remote( host, port )

@pytest.mark.parametrize("get_connection", [("localhost", 5555)], indirect=True)
def test_send_message(get_connection):
        with pytest.raises(TypeError):
                get_connection.sendall("")

        
        